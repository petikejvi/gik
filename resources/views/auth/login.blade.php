@extends('layouts.app')

@section('head')
        <title>GIK | Login</title>

        <link rel="stylesheet" href="{{ asset('css/login.css') }}"> 


@endsection

@section('content')


        <video poster="https://www.bloggernaija.com/wp-content/uploads/2017/04/POSTER_movie_film_movies_posters_5120x3620.jpg" id="bgvid" playsinline autoplay muted loop>
        <source src="vids/trailer.webm" type="video/webm">
        <source src="vids/trailer.mp4" type="video/mp4">
        </video>




        <div class="row overlay" id="over">

          <div class="col-lg-2 col-sm-4 formholder">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab"  style="color: #2E2028;" href="#login" role="tab">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#register" style="color: #2E2028;" role="tab">Register</a>
              </li>
            </ul>


            <div class="tab-content">


              <div class="tab-pane active" id="login" role="tabpanel">
                <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login_form') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address:</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password:</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}"  style="color: #2E2028;">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
              </div>




              <div class="tab-pane" id="register" role="tabpanel">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('register_email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="register_email" value="{{ old('register_email') }}" required>

                                @if ($errors->has('register_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('register_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('register_password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="register_password" required>

                                @if ($errors->has('register_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('register_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="register_password_confirmation" required>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('category1') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <label for="category1">Select category:</label>
                                  <select class="form-control" id="category1"  name="category1" >
                                    <option id="1" value="1">Action</option>
                                    <option id="2"  value="2" disabled>Drama</option>
                                    <option id="3"  value="3">Thriller</option>
                                    <option id="4"  value="4">Fantasy</option>
                                    <option id="5"  value="5">Horror</option>
                                    <option id="6"  value="6">Comedy</option>
                                  </select>
                                @if ($errors->has('category1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('category2') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <label for="category2">Select category:</label>
                                  <select class="form-control" id="category2"  name="category2" >
                                    <option id="1_1"  value="1" disabled>Action</option>
                                    <option id="2_2"  value="2">Drama</option>
                                    <option id="3_3"  value="3">Thriller</option>
                                    <option id="4_4"  value="4">Fantasy</option>
                                    <option id="5_5"  value="5">Horror</option>
                                    <option id="6_6"  value="6">Comedy</option>
                                  </select>
                                @if ($errors->has('category2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>              </div>
          </div>
        </div>
      </div>
<script type="text/javascript" language="javascript">
    
        $('#category1').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i + '_' + i).prop('disabled',false);
            }
            var value = $('#category1 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i + '_' + i).prop('disabled',true);
                }
            }

        });
        $('#category2').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i).prop('disabled',false);
            }
            var value = $('#category2 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i).prop('disabled',true);
                }
            }

        });
        $('#over').height($( window ).height() - 50);
</script>

@endsection
