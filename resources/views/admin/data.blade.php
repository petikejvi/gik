@extends('layouts.app', $notifications)

@section('head')
        <title> {{  __('gen.data') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="{{ asset('css/friendProfile.css') }}"> 
@endsection


@section('content')
        
 <div class="wrapper">

    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6" style="overflow-y: auto;">
            <div class="name text-center">
                <h2 style="color: white;">User data</h2>
                <a href="{{URL::previous()}}"><p>Turn Back</p></a>
            </div>
            <div class="name text-center">
                <p style="color: white;">Simple users</p>
                <p>{{$data[0]}}</p>
                <br>
                <p style="color: white;">Admin users</p>
                <p>{{$data[1]}}</p>
                <br>
                <p style="color: white;">Total users</p>
                <p>{{$data[1] + $data[0]}}</p>
                <br>
                <p style="color: white;">Movies count</p>
                <p>{{$data[2]}}</p>
                <br>
                <p style="color: white;">Friendship requests sent</p>
                <p>{{$data[3]}}</p>
                <br>
                <p style="color: white;">Suggestions sent</p>
                <p>{{$data[4]}}</p>
            </div>
        </div>

        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
</script>

@endsection
