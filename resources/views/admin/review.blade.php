@extends('layouts.app', $notifications)

@section('head')
        <title>Review Movies</title>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="{{ asset('css/friendProfile.css') }}"> 

        <style type="text/css">
            .wrapper{
                overflow-y: auto;
            }
        </style>


@endsection


@section('content')
        
 <div class="wrapper">

    <div class="row">
        @if($errors->has('success'))
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
        @if ($errors->has('error'))
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="contain watchHistory">
                <br>
                <h3>Featured movies</h3>
                <ul class="scrollableList">
                     @if(count($featured) == 0)
                    <p>{!!__('gen.no_data')!!}</p>
                    @else
                    @foreach($featured as $f)
                    <a href="{{url('/movie/watch/' . $f->id)}}">
                        <li class="history">
                           <p class="Mname">{{$f->title}}</p>
                           <div class=" thumbnail" style="background-image: url({{asset('img/'. $f->image. '')}});">
                           </div>
                           <form  style="display: inline !important;"  method="POST" action="{{url('/admin/featuredRemove')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="movie" value="{{$f->id}}">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-minus-sign"></span></button>
                        </form>
                        </li>
                    </a><br>
                    @endforeach
                    @endif
                </ul>
                <br>
            </div>

            <div class="contain watchHistory">
                <br>
                <h3>Movie List</h3>
                <ul class="scrollableList">
                     @if(count($movies) == 0)
                    <p>{!!__('gen.no_data')!!}</p>
                    @else
                    <input style="width: 20%;" id="search" class="form-control" type="text" name="name" placeholder="Search"><br>
                    <div id="movies">
                    @foreach($movies as $f)
                    <a href="{{url('/movie/watch/' . $f->id)}}">
                        <li class="history">
                           <p class="Mname">{{$f->title}}</p>
                           <div class=" thumbnail" style="background-image: url({{asset('img/'. $f->image. '')}});">
                           </div>
                           <form  style="display: inline !important;"  method="POST" action="{{url('/admin/movieDelete')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="movie" value="{{$f->id}}">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-remove"></span></button>
                            </form>
                           
                        <form  style="display: inline !important;"  method="POST" action="{{url('/admin/movieEdit')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="movie" value="{{$f->id}}">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-edit"></span></button>
                        </form>
                        <form  style="display: inline !important;"  method="POST" action="{{url('/admin/movieFeatured')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="movie" value="{{$f->id}}">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-plus-sign"></span></button>
                        </form>
                        </li>
                    </a><br>
                    @endforeach
                    </div>
                    @endif
                </ul>
                <br>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
        $('#over').height($( window ).height() - 50);
                            
        $(document).ready(function() {
        $('#search').keyup('submit', function () {

            var search = $('#search').val();

            $.ajax({
                type: "POST",
                url: "{{url('/admin/searchMovies')}}",
                data: {search: search},
                success: function( msg ) {
                    $('#movies').empty();
                    $('#movies').append(msg);
                }
            });
        });
    });
</script>

@endsection
