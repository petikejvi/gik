@extends('layouts.app', $notifications)

@section('head')
        <title> {{  Auth::user()->name }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="{{ asset('css/friendProfile.css') }}"> 
@endsection


@section('content')
        
 <div class="wrapper">

<div class="row">
        @if($errors->has('success'))
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
        @if($errors->has('error'))
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
    </div>

    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6">
            <div class="name text-center">
                <h2 style="color: white;">Add new Admin</h2>
                <form method="post" action="{{url('/admin/addAdmin')}}">
                    {{csrf_field()}}
                    <div class="col-sm-6">
                    <input class="form-control" type="email" name="email" placeholder="Admin's email">
                    </div>
                    <div class="col-sm-6">
                    <input class="form-control" type="submit" value="Go!"><br>
                    </div>
                </form>
                <a href="{{URL::previous()}}"><p>Turn Back</p></a>
            </div>


            <div class="contain watchHistory">
                <br>
                <h3>List of admins</h3>
                <ul class="scrollableList">
                     @if(count($admins) == 0)
                    <p>{!!__('gen.no_data')!!}</p>
                    @else
                    @foreach($admins as $a)
                    <a href="#">
                    <li class="history">
                       <p class="Mname">{{$a->name}}</p>
                        <form  style="display: inline !important;"  method="POST" action="{{url('/admin/removeAdmin')}} ">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$a->id}}">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-remove"></span></button>
                        </form>
                    </li></a><br>
                    @endforeach
                    @endif
                </ul>
                <br>
            </div>
        </div>

        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
</script>

@endsection
