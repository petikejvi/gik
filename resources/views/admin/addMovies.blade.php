@extends('layouts.app', $notifications)

@section('head')
        <title>Add Movie</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="{{ asset('css/friendProfile.css') }}"> 
@endsection


@section('content')
        
 <div class="wrapper">

    <div class="row">
        @if($errors->has('success'))
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
        @if ($errors->any() && !$errors->has('success'))
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{__('profile.fields_required')}}
        </div>
        @endif
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <br>
            <h1 class="text-center">Add Movie</h1><br>
            <form method="post" action="{{url('/admin/add')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="text" name="title" class="form-control" placeholder="Title" value="{{old('title')}}"><br>
                <textarea class="form-control" type="text-area" rows="4" name="description"  placeholder="Description">{{old('description')}}</textarea><br>
                <input class="form-control" type="text" name="url" placeholder="URL" value="{{old('url')}}"><br>
                <input style="width: 49%; display: inline;" class="form-control" type="date" name="release" value="{{old('release')}}">
                <input style="width: 49%; display: inline;" class="form-control" type="file" name="image" value="{{old('image')}}">
                <br><br>
                <input style="width: 49%; display: inline;"  class="form-control" type="text" name="rating" placeholder="Rating" value="{{old('rating')}}">
                <input style="width: 49%; display: inline;"  class="form-control" type="text" name="length" placeholder="Length" value="{{old('length')}}"><br><br>
                <select style="width: 49%; display: inline;" class="form-control" id="category1"  name="category1" value="{{old('category1')}}">
                    <option id="1" value="1">Action</option>
                    <option id="2"  value="2" disabled>Drama</option>
                    <option id="3"  value="3">Thriller</option>
                    <option id="4"  value="4">Fantasy</option>
                    <option id="5"  value="5">Horror</option>
                    <option id="6"  value="6">Comedy</option>
                </select>
                <select style="width: 49%; display: inline;" class="form-control" id="category2"  name="category2" value="{{old('category2')}}">
                    <option id="1_1"  value="1" disabled>Action</option>
                    <option id="2_2"  value="2">Drama</option>
                    <option id="3_3"  value="3">Thriller</option>
                    <option id="4_4"  value="4">Fantasy</option>
                    <option id="5_5"  value="5">Horror</option>
                    <option id="6_6"  value="6">Comedy</option>
                </select><br><br>
                <input class="btn btn-primary" type="submit" value="Add movie" name="submit">
                
            </form><br>
            <form action="{{url('/sendNotification')}}" method="get">
                <h4 class="text-center">Send a notification to all users that new movies were added</h4>
                <input class="btn btn-primary" type="submit" value="Send Notification" name="submit">
            </form>
        </div>
        <div class="col-sm-4"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
    
        $('#category1').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i + '_' + i).prop('disabled',false);
            }
            var value = $('#category1 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i + '_' + i).prop('disabled',true);
                }
            }

        });
        $('#category2').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i).prop('disabled',false);
            }
            var value = $('#category2 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i).prop('disabled',true);
                }
            }

        });
        $('#over').height($( window ).height() - 50);
</script>

@endsection
