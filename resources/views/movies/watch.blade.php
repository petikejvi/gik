@extends('layouts.app', $notifications)

@section('head')
@if($movie)
        <title>Watch | {{ $movie->title }}</title>
@endif
        <link rel="stylesheet" href="{{ asset('css/watchblade.css') }}"> 
@endsection


@section('content')
        
        <div class="row text-center" id="cont">
        @if($errors->has('success'))
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
        @if($errors->has('error'))
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          {{$errors->first()}}
        </div>
        @endif
                <div class="row" id="back" style="background-image: url('{{asset('img/'. $movie->id. '.jpg')}}');">
                </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12 center">
                        <h1>{{ $movie->title }}</h1>
                        {!! $movie->url !!}
                    </div>
                </div>
                    <br><br>
                <div class="row">
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-8" id="contain">
                        <div class="row">
                            <div class="col-md-6"  id="content">
                                <p>{{ $movie->description }}</p>
                            </div>
                            <div class="col-md-6" id="content1">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p>Release date: {{$movie->release}}</p>
                                        <p>Rating: {{$movie->rating}}</p>
                                        <p>Length: {{$movie->length}}</p>
                                        <p>Category: {{ $category1 }} , {{$category2}}</p>
                                    </div>
                                    
                                    <div class="col-md-12" style="padding: 0;">
                                        <form method="post" action="{{url('/suggest')}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="movie" value="{{$movie->id}}">
                                            <input class="form-control" style="width: auto; display: inline-block;" type="email" name="email" placeholder="Friend">
                                            <input class="btn btn-primary" style="width: auto; display: inline-block;" type="submit" value="Suggest">
                                        </form>
                                        <form method='post' action="{{url('/addToWatchLater')}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="movie" value="{{$movie->id}}">
                                            <button class="btn btn-primary" style="width: auto; display: inline-block; margin-top: 5px; float: right;" type="submit"><span class="glyphicon glyphicon-time"></span></button>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                    </div>
                </div>
                <br><br>
            </div>
        </div>

    <script type="text/javascript">
            $('iframe').width($( window ).width()/1.51).height($( window ).height()/1.51);
            $(document).ready(function() {
                var a = $('#content1').height();
                var b = $('#content').height();
                if(b>a){
                    $('#content1').height($('#content').height()-21);
                }
            });

            

    </script>

@endsection
