@extends('layouts.app', $notifications)

@section('head')
        <title>GIK | Search</title>
@endsection


@section('content')
  
      <div class="wrapper">
        <div class="row">
            @if(count($results)==0)
            <div class="col-sm-4"></div><div class="col-sm-4"><h4>{!!__('gen.no_results')!!}</h4></div><div class="col-sm-4"></div>
            @else
          <?php $i=0; $j = 0;  ?>
          @foreach($results as $result)
              @if($i==0)
              <div class="row">
              <div class="col-sm-2"></div>
              @endif
                    <a href="{{url('/movie/watch/' . $result->id)}}">
                  <div class="col-sm-2">
                    <div class="search_result" style="background-image: url('{{asset('img/'. $result->id. '.jpg')}}');">
                  </div><h3>{{$result->title}}</h3></div></a>
                  <?php $i++; $j++; ?>
              @if($i==4 || $j == count($results) )

              <div class="col-sm-2"></div>
              </div>
              @endif
            @endforeach
          
          @endif

        </div>
      </div>
        <style type="text/css">
            .search_result{
                height: 200px;
                width: 200px;
                background-size: cover;
                margin-top: 50px;
            }
            h3{
                color: white;
                text-align: center;
            }
            body{
              background-color: black;
              min-height: 100vh;
            }
            h4{
              color : white;
              text-align: center;
              vertical-align: center;
              padding-top: 30vh;
              line-height: 200%;
            }
            .wrapper{
              min-height: 100vh;
              background: linear-gradient(to bottom right, rgba(201,95,141,0.3) , rgba(32,143,162,0.3));
              background-repeat: no-repeat;
              background-size: cover;
            }
        </style>

@endsection
