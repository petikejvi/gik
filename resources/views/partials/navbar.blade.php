        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"  id="nav-glyph" style="color: white;">G!k</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <form class="navbar-form navbar-right">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search">
                </div>
              </form>
              <ul class="nav navbar-nav navbar-right">
                <!----------  User ------------------>
                <li><a href="#"><span id="nav-glyph" class="glyphicon glyphicon-user"></span></a></li>
                </li>
                <!-------------------- Notifications -------------------->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="nav-glyph" class="glyphicon glyphicon-globe"></span></a>
                  <ul class="dropdown-menu">
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-film"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Rejoice! New movies are here!</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                       <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-user"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Yooho! Filan just suggested a movie for you!</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-film"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Rejoice! New movies are here!</span>
                            </div>
                        </div>
                  </ul>
                </li>
                <!-------------  Categories -------------->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="nav-glyph" class="glyphicon glyphicon glyphicon-th-list"></span></a>
                  <ul class="dropdown-menu">
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Action</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Comedy</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Drama</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Fantasy</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Thriller</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Horror</span>
                            </div>
                        </div>
                    </li>
                  </ul>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
