@extends('layouts.app', $notifications)

@section('head')
        <title> {{  $friend[0]->name }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="{{ asset('css/friendProfile.css') }}"> 
@endsection


@section('content')
        
 <div class="wrapper">
    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6">
            <div class="name text-center">
            <h1>{{$friend[0]->name}}</h1>
            <p>Favorite categories:  {{ucfirst($category[0])}} , {{ucfirst($category[1])}}</p>
            <a href="{{URL::previous()}}"><p>Turn Back</p></a>
            </div>
            <div class="contain watchHistory">
                <br>
                <h3>Watched History</h3>
                <ul class="scrollableList">
                     @if(count($history) == 0)
                    <p>{!!__('gen.no_data')!!}</p>
                    @else
                    @foreach($history as $h)
                    <a href="{{url('/movie/watch/' . $h->id)}}">
                    <li class="history">
                       <p class="Mname">{{$h->title}}</p><div class=" thumbnail" style="background-image: url({{asset('img/'. $h->id. '.jpg')}});"></div>
                    </li></a><br>
                    @endforeach
                    @endif
                </ul>
                <br>
            </div>

            <div class="contain watchLater">
                 <br>
                <h3>Watch Later</h3>
                <ul class="scrollableList">
                    @if(count($watch) == 0)
                    <p>{!!__('gen.no_data')!!}</p>
                    @else
                    @foreach($watch as $w)
                    <a href="{{url('/movie/watch/' . $w->id)}}">
                    <li class="history">
                       <p class="Mname">{{$w->title}}</p><div class=" thumbnail" style="background-image: url({{asset('img/'. $w->id. '.jpg')}});"></div>
                    </li></a><br>
                    @endforeach
                    @endif
                </ul>
            </div>

        </div>

        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
</script>

@endsection
