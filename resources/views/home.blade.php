@extends('layouts.app', $notifications)

@section('head')
        <title>GIK | Home</title>
@endsection


@section('content')

<div class="row" id="over">
            <?php $i = 1; ?>
            @foreach ($featured as $feature)
            @if($i<5)
                <a href="{{url('/movie/watch/' . $feature->id)}}">
                <div class="col-sm-3 featured-images" id="{{ $i }}" style="background-image: url('{{asset('img/'. $feature->image . '')}}');">
                </div>
                <?php $i++ ?>
            @endif
            @endforeach


        </div>

         <script type="text/javascript">
                    $('.featured-images').height($(window).height());
                    // Zoom and slide effect
                    $('#1').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#3').animate({width : "20%", easing: "easeout"}, 400);
                        $('#2').animate({width : "20%", easing: "easeout"}, 400);
                        $('#4').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                    }
                    });
                    $('#2').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#1').animate({width : "20%", easing: "easeout"}, 400);
                        $('#3').animate({width : "20%", easing: "easeout"}, 400);
                        $('#4').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                        }
                    });
                    $('#3').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#1').animate({width : "20%", easing: "easeout"}, 400);
                        $('#2').animate({width : "20%", easing: "easeout"}, 400);
                        $('#4').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                        }
                    });
                    $('#4').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#1').animate({width : "20%", easing: "easeout"}, 400);
                        $('#2').animate({width : "20%", easing: "easeout"}, 400);
                        $('#3').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                        }
                    });

                    $('#over').height($( window ).height() - 50);



            </script>

            <style type="text/css">
                #over{
                    max-height: 94% !important;
                }
                .featured-images{
                    padding: 0px;
                    margin: 0px;
                    background-size: cover;
                    background-position: center;
                    height: 95%;
                }
            </style>

@endsection
