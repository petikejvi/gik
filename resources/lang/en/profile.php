<?php

return [

   
    'password_missmatch' => 'HIMOS Spell-Checking Quill suggests that your password missmatch',
    'update' => 'HIMOS Scouring Charm has tidied up your profile!',
    'not_found' => 'Sorry, but HIMOS owl could not find the desired individual.',
    'already_friends' => 'HIMOS Homenum revelio spell suggests that you are already friends.',
    'self_friend' => 'Sorry, but HIMOS love potion can not befriend you with yourself.',
    'befriended' => 'HIMOS successfully sent an frendship letter with an owl',
    'unfriend' => 'HIMOS cast a Relashio spell and now you are no longer friends!',
    'accept' => 'HIMOS love potion worked! You are now friends!',
    'notFound' => 'HIMOS Alohomora spell cannot break into someones profile if you are no friends yet!',
    'already_admin' => 'HIMOS Felix Felicis potion could not make the individual an admin because he already is!',
    'made_admin' => 'HIMOS Felix Felicis potion worked! He is now a successful admin!',
    'remove_admin' => 'HIMOS cast a Relashio spell and the individual is no longer admin!',
    'unable_admin' => 'HIMOS got confused with this request, something is not right, dark forces lurk by!',
    'self_u`nadmin' => 'It\'s beyond HIMOS wand power to make you remove such priviledge from yourself...',
    'admin_yourself' => 'HIMOS saw you from its crystal ball and says that you are already here, funny what you tried though!',
    'movie_added' => 'HIMOS stored the movie successfully',
    'movie_notadded' => 'HIMOS was unable to store the movie!',
    'fields_required' => 'HIMOS suggests to double check your data!',
    'featured_removed' => 'HIMOS Expelliamus spell removed successfully the featured movie',
    'featured_added' => 'HIMOS accio spell worked, featured movie was added successfully!',
    'couldnt_add' => 'HIMOS was unable to add the movie, for it counts 4 already selected',
    'already_added' => 'HIMOS cannot add the same movie twice!',
    'movie_deleted' => 'HIMOS cast the unforgivable curse, Havada Kedavra! The movie is now gone!',
    'movie_updated' => 'HIMOS put everything nice and neat successfully',
    'notifications_sent' => 'Horray! HIMOS sent an owl to everyone to let them know for new movie arrivals!'
 
];