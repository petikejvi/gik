<?php

return [

   
    'no_results' => 'Upsie! <br> HIMOS ( our Higly Inteligent Movie Organizer System ) was hit by Obliviate spell and could not find what you were searching.<br>Please try something else!',
    'history' => 'Watch History',
    'suggested' => 'Suggested Movies',
    'to_watch' => 'Watch Later',
    'profile' => 'Profile',
    'friends' => 'Friends',
    'prefered' => 'Prefered Categories',
    'no_data' => 'Upsie! <br> HIMOS\'s Accio spell could not fetch anything!',
    'home' => 'Gik | Home',
    'data' => 'General Data',
 
];