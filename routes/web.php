<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@home')->name('home');

Route::get('/movie/watch/{id}', 'MovieController@getMovie');

Route::post('/notif', 'NotificationController@resetNotifications')->name('reset_notifications');

Route::get('/search', 'SearchController@search')->name('search');

Route::get('/category/{category}', 'SearchController@category')->name('search1');

Route::get('/profile', 'ProfileController@index');

Route::post('/profile/profileUpdate', 'ProfileController@update');

Route::post('/profile/friends', 'ProfileController@setFriends');

Route::post('/friend/remove', 'ProfileController@removeFriend');

Route::post('/friend/add', 'ProfileController@addFriend');

Route::post('/suggest', 'SuggestionController@suggest');

Route::post('/addToWatchLater', 'MovieController@addWatchLater');

Route::get('/profile/{id}' , 'ProfileController@friendProfile');



Route::get('/admin/user' , 'AdminController@getAdmin');

Route::post('/admin/addAdmin' , 'AdminController@addAdmin');

Route::post('/admin/removeAdmin' , 'AdminController@removeAdmin');

Route::get('/admin/data' , 'AdminController@userData');

Route::get('/admin/movies' , 'AdminController@addMovies');

Route::post('/admin/add' , 'AdminController@insertMovies');

Route::get('/admin/review' , 'AdminController@reviewMovies');

Route::post('/admin/featuredRemove' , 'AdminController@featuredRemove');

Route::post('/admin/searchMovies' , 'AdminController@searchMovies');

Route::post('/admin/movieDelete' , 'AdminController@movieDelete');

Route::post('/admin/movieEdit' , 'AdminController@movieEdit');

Route::post('/admin/movieFeatured' , 'AdminController@movieFeatured');

Route::post('/admin/editData' , 'AdminController@editData');

Route::get('/sendNotification', 'AdminController@sendMovieNotification');