<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/head.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/body.css')); ?>"> 
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
        <meta name="_token" content="<?php echo csrf_token(); ?>" /> 


        <?php echo $__env->yieldContent('head'); ?>


        </head>
        <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo e(route('home')); ?>"  id="nav-glyph" style="color: white;">G!k</a>
            </div>


            <?php if(Auth::check()): ?>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <form class="navbar-form navbar-right" action="<?php echo e(route('search')); ?>" method="GET">
                <div class="form-group">
                  <input type="text" class="form-control" name="search" placeholder="Search">
                </div>
              </form>
              <ul class="nav navbar-nav navbar-right">
                <!----------  User ------------------>
                <a href="<?php echo e(url('/profile')); ?>">
                <li><span id="nav-glyph" class="glyphicon glyphicon-user"></span></a></li>
                </a>
                <!-------------------- Notifications -------------------->
                <li class="dropdown">
                  <a href="#" id="notif" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="nav-glyph" class="glyphicon glyphicon-globe"></span>

                  <?php
                        $i = 0;
                      foreach($notifications as $notif){
                          if($notif->read == false){
                          $i++;
                          }
                      }
                  ?>

                  <?php if(count($notifications) > 0 && $i != 0): ?>
                  <span class="badge"><?php echo e($i); ?></span>
                  <?php endif; ?>
                  </a>
                  <ul class="dropdown-menu">

                 <?php if(count($notifications) == 0): ?>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-user"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Upssie, No notifications yet!</span>
                            </div>
                        </div>
                    </li>
                <?php endif; ?>

                <?php $__currentLoopData = $notifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $notification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($notification->category == 1): ?>    
                    <a href="<?php echo e($notification->link); ?>">
                <?php else: ?>
                    <a href="<?php echo e(url('/profile')); ?>">
                <?php endif; ?>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-2 notif text-center">

                <?php if($notification->category ==1): ?>                
                                <span id="notif-gly" class="glyphicon glyphicon-film"></span>
                <?php else: ?>
                                <span id="notif-gly" class="glyphicon glyphicon-user"></span>
                <?php endif; ?>
                            </div>
                            <div class="col-xs-10 notif">
                                <span><?php echo e($notification->message); ?>!</span>
                            </div>
                        </div>
                    </li> 
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                <!-------------  Categories -------------->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="nav-glyph" class="glyphicon glyphicon glyphicon-th-list"></span></a>
                  <ul class="dropdown-menu">
                    <a href="<?php echo e(url('/category/action')); ?>">
                    <li  id="categories">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Action</span>
                            </div>
                        </div>
                    </li>
                    </a>
                    <a href="<?php echo e(url('/category/comedy')); ?>">
                    <li  id="categories">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Comedy</span>
                            </div>
                        </div>
                    </li>
                    </a>
                    <a href="<?php echo e(url('/category/drama')); ?>">
                    <li  id="categories">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Drama</span>
                            </div>
                        </div>
                    </li>
                    </a>
                    <a href="<?php echo e(url('/category/fantasy')); ?>">
                    <li  id="categories">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Fantasy</span>
                            </div>
                        </div>
                    </li>
                    </a>
                    <a href="<?php echo e(url('/category/thriller')); ?>">
                    <li  id="categories">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Thriller</span>
                            </div>
                        </div>
                    </li>
                    </a>
                    <a href="<?php echo e(url('/category/horror')); ?>">
                    <li  id="categories">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Horror</span>
                            </div>
                        </div>
                    </li>
                    </a>
                  </ul>
                  <li><a href="<?php echo e(route('logout')); ?>"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span id="nav-glyph" class="glyphicon glyphicon-off"></span></a></li>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->

            <?php endif; ?>

          </div><!-- /.container-fluid -->
        </nav>  
        <form style="display: none; visibility: hidden;" id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;"><?php echo e(csrf_field()); ?></form>
        <?php echo $__env->yieldContent('content'); ?>

    <?php if(Auth::check()): ?>  

        <script type="text/javascript">
            jQuery(document).ready(function($)  {
             $.ajaxSetup({
                        headers: { 'X-CSRF-Token' : $('meta[name="_token"]').attr('content') }
                    });
                });
            var data = 0;
                            
            $('#notif').click(function () {
                if(data == 0){
                    $('.badge').fadeOut('fast');
                    var dataString = 'hi';
                    var url = "<?php echo e(route('reset_notifications')); ?>" ;


                    $.ajax({
                        type: "POST",
                        url: url,
                        data:dataString,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function(data) {
                            console.log(data);
                            console.log("success");
                        },
                        error: function(data) {
                            console.log(data);
                            console.log("error");
                        }

                    });
                    data = 1;
                }
                else{
                    data = 0;
                }
            });
    <?php endif; ?>
        </script>
</body>
</html>

