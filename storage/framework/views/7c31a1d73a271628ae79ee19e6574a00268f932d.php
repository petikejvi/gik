<?php $__env->startSection('head'); ?>
        <title> <?php echo e(Auth::user()->name); ?></title>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/friendProfile.css')); ?>"> 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
        
 <div class="wrapper">

<div class="row">
        <?php if($errors->has('success')): ?>
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <?php if($errors->has('error')): ?>
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6">
            <div class="name text-center">
                <h2 style="color: white;">Add new Admin</h2>
                <form method="post" action="<?php echo e(url('/admin/addAdmin')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <div class="col-sm-6">
                    <input class="form-control" type="email" name="email" placeholder="Admin's email">
                    </div>
                    <div class="col-sm-6">
                    <input class="form-control" type="submit" value="Go!"><br>
                    </div>
                </form>
                <a href="<?php echo e(URL::previous()); ?>"><p>Turn Back</p></a>
            </div>


            <div class="contain watchHistory">
                <br>
                <h3>List of admins</h3>
                <ul class="scrollableList">
                     <?php if(count($admins) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $admins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="#">
                    <li class="history">
                       <p class="Mname"><?php echo e($a->name); ?></p>
                        <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/admin/removeAdmin')); ?> ">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="id" value="<?php echo e($a->id); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-remove"></span></button>
                        </form>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
                <br>
            </div>
        </div>

        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>