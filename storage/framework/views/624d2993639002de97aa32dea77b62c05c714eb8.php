<?php $__env->startSection('head'); ?>
        <title> <?php echo e(Auth::user()->name); ?></title>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/friendProfile.css')); ?>"> 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
        
 <div class="wrapper">
    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6">
            <div class="name text-center">
            <h1><?php echo e($friend[0]->name); ?></h1>
            <p>Favorite categories:  <?php echo e(ucfirst($category[0])); ?> , <?php echo e(ucfirst($category[1])); ?></p>
            <a href="<?php echo e(URL::previous()); ?>"><p>Turn Back</p></a>
            </div>
            <div class="contain watchHistory">
                <br>
                <h3>Watched History</h3>
                <ul class="scrollableList">
                     <?php if(count($history) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $h): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $h->id)); ?>">
                    <li class="history">
                       <p class="Mname"><?php echo e($h->title); ?></p><div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $h->id. '.jpg')); ?>);"></div>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
                <br>
            </div>

            <div class="contain watchLater">
                 <br>
                <h3>Watch Later</h3>
                <ul class="scrollableList">
                    <?php if(count($watch) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $watch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $w): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $w->id)); ?>">
                    <li class="history">
                       <p class="Mname"><?php echo e($w->title); ?></p><div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $w->id. '.jpg')); ?>);"></div>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </div>

        </div>

        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>