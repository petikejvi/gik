<?php $__env->startSection('head'); ?>
        <title> <?php echo e(__('gen.data')); ?></title>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/friendProfile.css')); ?>"> 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
        
 <div class="wrapper">

    <div class="row">
        <div class="col-sm-3"></div>

        <div class="col-sm-6" style="overflow-y: auto;">
            <div class="name text-center">
                <h2 style="color: white;">User data</h2>
                <a href="<?php echo e(URL::previous()); ?>"><p>Turn Back</p></a>
            </div>
            <div class="name text-center">
                <p style="color: white;">Simple users</p>
                <p><?php echo e($data[0]); ?></p>
                <br>
                <p style="color: white;">Admin users</p>
                <p><?php echo e($data[1]); ?></p>
                <br>
                <p style="color: white;">Total users</p>
                <p><?php echo e($data[1] + $data[0]); ?></p>
                <br>
                <p style="color: white;">Movies count</p>
                <p><?php echo e($data[2]); ?></p>
                <br>
                <p style="color: white;">Friendship requests sent</p>
                <p><?php echo e($data[3]); ?></p>
                <br>
                <p style="color: white;">Suggestions sent</p>
                <p><?php echo e($data[4]); ?></p>
            </div>
        </div>

        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>