<?php $__env->startSection('head'); ?>
        <title>GIK | Home</title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

<div class="row" id="over">
            <?php $i = 1; ?>
            <?php $__currentLoopData = $featured; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $feature): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($i<5): ?>
                <a href="<?php echo e(url('/movie/watch/' . $feature->id)); ?>">
                <div class="col-sm-3 featured-images" id="<?php echo e($i); ?>" style="background-image: url('<?php echo e(asset('img/'. $feature->image . '')); ?>');">
                </div>
                <?php $i++ ?>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


        </div>

         <script type="text/javascript">
                    $('.featured-images').height($(window).height());
                    // Zoom and slide effect
                    $('#1').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#3').animate({width : "20%", easing: "easeout"}, 400);
                        $('#2').animate({width : "20%", easing: "easeout"}, 400);
                        $('#4').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                    }
                    });
                    $('#2').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#1').animate({width : "20%", easing: "easeout"}, 400);
                        $('#3').animate({width : "20%", easing: "easeout"}, 400);
                        $('#4').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                        }
                    });
                    $('#3').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#1').animate({width : "20%", easing: "easeout"}, 400);
                        $('#2').animate({width : "20%", easing: "easeout"}, 400);
                        $('#4').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                        }
                    });
                    $('#4').mouseover(function(){
                        var windowWidth = $(window).width();
                        if(windowWidth > 768){
                        $('#1').animate({width : "20%", easing: "easeout"}, 400);
                        $('#2').animate({width : "20%", easing: "easeout"}, 400);
                        $('#3').animate({width : "20%", easing: "easeout"}, 400);
                        $(this).animate({width : "40%", easing: "easeout"}, 400);
                        }
                    });

                    $('#over').height($( window ).height() - 50);



            </script>

            <style type="text/css">
                #over{
                    max-height: 94% !important;
                }
                .featured-images{
                    padding: 0px;
                    margin: 0px;
                    background-size: cover;
                    background-position: center;
                    height: 95%;
                }
            </style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>