<?php $__env->startSection('head'); ?>


        <link rel="stylesheet" href="<?php echo e(asset('css/login.css')); ?>"> 


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


        <video poster="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/polina.jpg" id="bgvid" playsinline autoplay muted loop>
        <source src="vids/trailer.webm" type="video/webm">
        <source src="vids/trailer.mp4" type="video/mp4">
        </video>




        <div class="row overlay">

          <div class="col-lg-2 col-sm-4 formholder">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#login" role="tab">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#register" role="tab">Register</a>
              </li>
            </ul>


            <div class="tab-content">


              <div class="tab-pane active" id="login" role="tabpanel">
                <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo e(csrf_field()); ?>


                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-4 control-label">E-Mail Address:</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" required autofocus>

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-4 control-label">Password:</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
              </div>




              <div class="tab-pane" id="register" role="tabpanel">
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo e(csrf_field()); ?>


                        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="<?php echo e(old('name')); ?>" required autofocus>

                                <?php if($errors->has('name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('register_email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="register_email" value="<?php echo e(old('register_email')); ?>" required>

                                <?php if($errors->has('register_email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('register_email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('register_password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="register_password" required>

                                <?php if($errors->has('register_password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('register_password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="register_password_confirmation" required>
                            </div>
                        </div>


                        <div class="form-group<?php echo e($errors->has('category1') ? ' has-error' : ''); ?>">
                            <div class="col-md-6">
                                <label for="category1">Select category:</label>
                                  <select class="form-control" id="category1"  name="category1" >
                                    <option id="1" value="1">Action</option>
                                    <option id="2"  value="2" disabled>Drama</option>
                                    <option id="3"  value="3">Thriller</option>
                                    <option id="4"  value="4">Fantasy</option>
                                    <option id="5"  value="5">Horror</option>
                                    <option id="6"  value="6">Comedy</option>
                                  </select>
                                <?php if($errors->has('category1')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('category1')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>



                        <div class="form-group<?php echo e($errors->has('category2') ? ' has-error' : ''); ?>">
                            <div class="col-md-6">
                                <label for="category2">Select category:</label>
                                  <select class="form-control" id="category2"  name="category2" >
                                    <option id="1_1"  value="1" disabled>Action</option>
                                    <option id="2_2"  value="2">Drama</option>
                                    <option id="3_3"  value="3">Thriller</option>
                                    <option id="4_4"  value="4">Fantasy</option>
                                    <option id="5_5"  value="5">Horror</option>
                                    <option id="6_6"  value="6">Comedy</option>
                                  </select>
                                <?php if($errors->has('category2')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('category2')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>              </div>
          </div>
        </div>
      </div>
<script type="text/javascript" language="javascript">
    
        $('#category1').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i + '_' + i).prop('disabled',false);
            }
            var value = $('#category1 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i + '_' + i).prop('disabled',true);
                }
            }

        });
        $('#category2').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i).prop('disabled',false);
            }
            var value = $('#category2 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i).prop('disabled',true);
                }
            }

        });
</script>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>