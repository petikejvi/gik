<?php $__env->startSection('head'); ?>
        <title>Review Movies</title>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/friendProfile.css')); ?>"> 

        <style type="text/css">
            .wrapper{
                overflow-y: auto;
            }
        </style>


<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
        
 <div class="wrapper">

    <div class="row">
        <?php if($errors->has('success')): ?>
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <?php if($errors->has('error')): ?>
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="contain watchHistory">
                <br>
                <h3>Featured movies</h3>
                <ul class="scrollableList">
                     <?php if(count($featured) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $featured; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $f->id)); ?>">
                        <li class="history">
                           <p class="Mname"><?php echo e($f->title); ?></p>
                           <div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $f->image. '')); ?>);">
                           </div>
                           <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/admin/featuredRemove')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="movie" value="<?php echo e($f->id); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-minus-sign"></span></button>
                        </form>
                        </li>
                    </a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
                <br>
            </div>

            <div class="contain watchHistory">
                <br>
                <h3>Movie List</h3>
                <ul class="scrollableList">
                     <?php if(count($movies) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <input style="width: 20%;" id="search" class="form-control" type="text" name="name" placeholder="Search"><br>
                    <div id="movies">
                    <?php $__currentLoopData = $movies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $f->id)); ?>">
                        <li class="history">
                           <p class="Mname"><?php echo e($f->title); ?></p>
                           <div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $f->image. '')); ?>);">
                           </div>
                           <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/admin/movieDelete')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="movie" value="<?php echo e($f->id); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-remove"></span></button>
                            </form>
                           
                        <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/admin/movieEdit')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="movie" value="<?php echo e($f->id); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-edit"></span></button>
                        </form>
                        <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/admin/movieFeatured')); ?>">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="movie" value="<?php echo e($f->id); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-plus-sign"></span></button>
                        </form>
                        </li>
                    </a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php endif; ?>
                </ul>
                <br>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>

 </div>
<script type="text/javascript" language="javascript">
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
        $('.wrapper').height($( window ).height()- 50);
        $('#over').height($( window ).height() - 50);
                            
        $(document).ready(function() {
        $('#search').keyup('submit', function () {

            var search = $('#search').val();

            $.ajax({
                type: "POST",
                url: "<?php echo e(url('/admin/searchMovies')); ?>",
                data: {search: search},
                success: function( msg ) {
                    $('#movies').empty();
                    $('#movies').append(msg);
                }
            });
        });
    });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>