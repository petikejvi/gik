<?php $__env->startSection('head'); ?>
<?php if($movie): ?>
        <title>Watch | <?php echo e($movie->title); ?></title>
<?php endif; ?>
        <link rel="stylesheet" href="<?php echo e(asset('css/watchblade.css')); ?>"> 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
        
        <div class="row text-center" id="cont">
        <?php if($errors->has('success')): ?>
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <?php if($errors->has('error')): ?>
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
                <div class="row" id="back" style="background-image: url('<?php echo e(asset('img/'. $movie->id. '.jpg')); ?>');">
                </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12 center">
                        <h1><?php echo e($movie->title); ?></h1>
                        <?php echo $movie->url; ?>

                    </div>
                </div>
                    <br><br>
                <div class="row">
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-8" id="contain">
                        <div class="row">
                            <div class="col-md-6"  id="content">
                                <p><?php echo e($movie->description); ?></p>
                            </div>
                            <div class="col-md-6" id="content1">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p>Release date: <?php echo e($movie->release); ?></p>
                                        <p>Rating: <?php echo e($movie->rating); ?></p>
                                        <p>Length: <?php echo e($movie->length); ?></p>
                                        <p>Category: <?php echo e($category1); ?> , <?php echo e($category2); ?></p>
                                    </div>
                                    
                                    <div class="col-md-12" style="padding: 0;">
                                        <form method="post" action="<?php echo e(url('/suggest')); ?>">
                                            <?php echo e(csrf_field()); ?>

                                            <input type="hidden" name="movie" value="<?php echo e($movie->id); ?>">
                                            <input class="form-control" style="width: auto; display: inline-block;" type="email" name="email" placeholder="Friend">
                                            <input class="btn btn-primary" style="width: auto; display: inline-block;" type="submit" value="Suggest">
                                        </form>
                                        <form method='post' action="<?php echo e(url('/addToWatchLater')); ?>">
                                            <?php echo e(csrf_field()); ?>

                                            <input type="hidden" name="movie" value="<?php echo e($movie->id); ?>">
                                            <button class="btn btn-primary" style="width: auto; display: inline-block; margin-top: 5px; float: right;" type="submit"><span class="glyphicon glyphicon-time"></span></button>
                                        </form>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                    </div>
                </div>
                <br><br>
            </div>
        </div>

    <script type="text/javascript">
            $('iframe').width($( window ).width()/1.51).height($( window ).height()/1.51);
            $(document).ready(function() {
                var a = $('#content1').height();
                var b = $('#content').height();
                if(b>a){
                    $('#content1').height($('#content').height()-21);
                }
            });

            

    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>