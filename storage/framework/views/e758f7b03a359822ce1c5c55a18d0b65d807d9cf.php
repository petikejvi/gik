<?php $__env->startSection('head'); ?>
        <title> <?php echo e(Auth::user()->name); ?></title>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/profile.css')); ?>"> 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
        
 <div class="wrapper">

    <div class="row">
        <?php if($errors->has('success')): ?>
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <?php if($errors->has('error')): ?>
        <div class="alert alert-danger alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <?php if($errors->has('friends')): ?>
        <div class="alert alert-warning alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <?php if($errors->has('friendship')): ?>
        <div class="alert alert-success alert-dismissable text-center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?php echo e($errors->first()); ?>

        </div>
        <?php endif; ?>
        <div class="col-sm-1">
        </div>

        <div class="col-sm-3 profileSection">
            <br>
            <h3>Profile</h3>
            <div class="containProfile">
                <h4>Name:  <?php echo e(Auth::user()->name); ?></h4>
                <h4>Categories chosen: <?php echo e($category[0]); ?> , <?php echo e($category[1]); ?></h4>
            </div>
            <h3>Change your data</h3>
            <form method="POST" action="<?php echo e(url('/profile/profileUpdate')); ?>">
                <?php echo e(csrf_field()); ?>

            <h4>Name:</h4>
            <input class="form-control" type="text" name="name" placeholder="Full Name">
            <h4>Email:</h4>
            <input class="form-control" type="email" name="email" placeholder="Email">
            <h4>Password:</h4>
            <input class="form-control" type="password" name="password" placeholder="Password">
            <h4>Repeat Password:</h4>
            <input class="form-control" type="password" name="confirm_password" placeholder="Password">
            <h4>First Category:</h4>
             <select class="form-control" id="category1"  name="category1" >
                <option id="1" value="1">Action</option>
                <option id="2"  value="2" disabled>Drama</option>
                <option id="3"  value="3">Thriller</option>
                <option id="4"  value="4">Fantasy</option>
                <option id="5"  value="5">Horror</option>
                <option id="6"  value="6">Comedy</option>
              </select>
               <h4>Second Category:</h4>
            <select class="form-control" id="category2"  name="category2" >
                <option id="1_1"  value="1" disabled>Action</option>
                <option id="2_2"  value="2">Drama</option>
                <option id="3_3"  value="3">Thriller</option>
                <option id="4_4"  value="4">Fantasy</option>
                <option id="5_5"  value="5">Horror</option>
                <option id="6_6"  value="6">Comedy</option>
              </select><br>
              <input class="btn btn-primary" type="submit" value="Change">
              <br>
          </form>
        </div>

        <div class="col-sm-7">

            <?php if(\Auth::user()->type == 1): ?>
            <div class="contain watchHistory">
                <br>
                <h3>Admin Panel</h3>
                <ul class="scrollableList">
                    <a href="<?php echo e(url('/admin/movies')); ?>">
                    <li class="history">
                       <p class="Mname">Add movies</p>
                    </li></a><br>
                    <a href="<?php echo e(url('/admin/review')); ?>">
                    <li class="history">
                       <p class="Mname">Review movies</p>
                    </li></a><br>
                    <a href="<?php echo e(url('/admin/data')); ?>">
                    <li class="history">
                       <p class="Mname">Review user data</p>
                    </li></a><br>
                    <a href="<?php echo e(url('/admin/user')); ?>">
                    <li class="history">
                       <p class="Mname">Review admins</p>
                    </li></a><br>
                </ul>
                <br>
            </div>
            <?php endif; ?>

            <div class="contain watchHistory">
                <br>
                <h3>Watched History</h3>
                <ul class="scrollableList">
                     <?php if(count($history) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $h): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $h->id)); ?>">
                    <li class="history">
                       <p class="Mname"><?php echo e($h->title); ?></p><div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $h->image. '')); ?>);"></div>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
                <br>
            </div>

            <div class="contain suggestedMovies">
                 <br>
                <h3>Suggested Movies</h3>
                <ul class="scrollableList">
                    <?php if(count($suggestions) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $suggestions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $s->id)); ?>">
                    <li class="history">
                       <p class="Mname"><?php echo e($s->title); ?> | From: <?php echo e($s->name); ?></p><div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $s->movie_id. '.jpg')); ?>);"></div>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </div>

            <div class="contain watchLater">
                 <br>
                <h3>Watch Later</h3>
                <ul class="scrollableList">
                    <?php if(count($watch) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $watch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $w): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/movie/watch/' . $w->id)); ?>">
                    <li class="history">
                       <p class="Mname"><?php echo e($w->title); ?></p><div class=" thumbnail" style="background-image: url(<?php echo e(asset('img/'. $w->image. '.jpg')); ?>);"></div>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </div>

            <div class="contain watchLater">
                 <br>
                <h3>Friends</h3>
                <form method="post" action="<?php echo e(url('/profile/friends')); ?>">
                    <?php echo e(csrf_field()); ?>

                <div class="col-sm-8">
                    <input class="form-control" type="email" name="email" placeholder="Friend's email">
                </div>
                <div class="col-sm-4">
                <input class="form-control" type="submit" value="Go!"><br>
                </form>
                </div>
                <ul class="scrollableList">
                    <?php if(count($friends) == 0): ?>
                    <p><?php echo __('gen.no_data'); ?></p>
                    <?php else: ?>
                    <?php $__currentLoopData = $friends; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $f): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(url('/profile/' . $f['id'])); ?>">
                    <li class="history">
                       <p class="Mname"><?php echo e($f['name']); ?></p></a>
                        <?php if($f['status'] == 1 || $f['status'] == 0): ?>
                        <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/friend/remove')); ?> ">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="friend_id" value="<?php echo e($f['id']); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-remove"></span></button>
                        </form>
                       <?php elseif($f['status'] == 2): ?>
                        <form  style="display: inline !important;"  method="POST" action="<?php echo e(url('/friend/add')); ?> ">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="friend_id" value="<?php echo e($f['id']); ?>">
                            <button  type="submit" class="btn btn-primary" style="width: auto; height: 100%; float: right;"><span class="glyphicon glyphicon-ok"></span></button>
                        </form>
                        <?php endif; ?>
                    </li></a><br>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </ul>
            </div>


        </div>
        <div class="col-sm-1">

    </div>


 </div>
<script type="text/javascript" language="javascript">
    
        $('#category1').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i + '_' + i).prop('disabled',false);
            }
            var value = $('#category1 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i + '_' + i).prop('disabled',true);
                }
            }

        });
        $('#category2').on('change' ,
        function(){
            for(var i = 1; i<7 ; i++){
                    $('#' + i).prop('disabled',false);
            }
            var value = $('#category2 option:selected').val();
            for(var i = 1; i<7 ; i++){
                if(value == i){
                    $('#' + i).prop('disabled',true);
                }
            }

        });
        $('.col-sm-7').height($( window ).height() - 50);
        $('body').height($( window ).height() - 100);
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>