<?php $__env->startSection('head'); ?>
        <title>GIK | Search</title>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
  
      <div class="wrapper">
        <div class="row">
            <?php if(count($results)==0): ?>
            <div class="col-sm-4"></div><div class="col-sm-4"><h4><?php echo __('gen.no_results'); ?></h4></div><div class="col-sm-4"></div>
            <?php else: ?>
          <?php $i=0; $j = 0;  ?>
          <?php $__currentLoopData = $results; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $result): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if($i==0): ?>
              <div class="row">
              <div class="col-sm-2"></div>
              <?php endif; ?>
                    <a href="<?php echo e(url('/movie/watch/' . $result->id)); ?>">
                  <div class="col-sm-2">
                    <div class="search_result" style="background-image: url('<?php echo e(asset('img/'. $result->id. '.jpg')); ?>');">
                  </div><h3><?php echo e($result->title); ?></h3></div></a>
                  <?php $i++; $j++; ?>
              <?php if($i==4 || $j == count($results) ): ?>

              <div class="col-sm-2"></div>
              </div>
              <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          
          <?php endif; ?>

        </div>
      </div>
        <style type="text/css">
            .search_result{
                height: 200px;
                width: 200px;
                background-size: cover;
                margin-top: 50px;
            }
            h3{
                color: white;
                text-align: center;
            }
            body{
              background-color: black;
              min-height: 100vh;
            }
            h4{
              color : white;
              text-align: center;
              vertical-align: center;
              padding-top: 30vh;
              line-height: 200%;
            }
            .wrapper{
              min-height: 100vh;
              background: linear-gradient(to bottom right, rgba(201,95,141,0.3) , rgba(32,143,162,0.3));
              background-repeat: no-repeat;
              background-size: cover;
            }
        </style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', $notifications, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>