<!doctype html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?php echo e(asset('css/head.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/body.css')); ?>"> 
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title><?php echo e(config('app.name', 'Laravel')); ?></title>

        <?php echo $__env->yieldContent('head'); ?>


        </head>
        <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"  id="nav-glyph" style="color: white;">G!k</a>
            </div>


            <?php if(Auth::check()): ?>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <form class="navbar-form navbar-right">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search">
                </div>
              </form>
              <ul class="nav navbar-nav navbar-right">
                <!----------  User ------------------>
                <li><a href="#"><span id="nav-glyph" class="glyphicon glyphicon-user"></span></a></li>
                </li>
                <!-------------------- Notifications -------------------->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="nav-glyph" class="glyphicon glyphicon-globe"></span></a>
                  <ul class="dropdown-menu">
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-film"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Rejoice! New movies are here!</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                       <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-user"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Yooho! Filan just suggested a movie for you!</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-2 notif text-center">
                                <span id="notif-gly" class="glyphicon glyphicon-film"></span>
                            </div>
                            <div class="col-xs-10 notif">
                                <span>Rejoice! New movies are here!</span>
                            </div>
                        </div>
                  </ul>
                </li>
                <!-------------  Categories -------------->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span id="nav-glyph" class="glyphicon glyphicon glyphicon-th-list"></span></a>
                  <ul class="dropdown-menu">
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Action</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Comedy</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Drama</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Fantasy</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Thriller</span>
                            </div>
                        </div>
                    </li>
                    <li  id="notifications">
                        <div class="row">
                            <div class="col-xs-12 notif">
                                <span>Horror</span>
                            </div>
                        </div>
                    </li>
                  </ul>
                  <li><a href="<?php echo e(route('logout')); ?>"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span id="nav-glyph" class="glyphicon glyphicon-off"></span></a></li>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->

            <?php endif; ?>

          </div><!-- /.container-fluid -->
        </nav>
        <form style="display: none; visibility: hidden;" id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;"><?php echo e(csrf_field()); ?></form>
        <?php echo $__env->yieldContent('content'); ?>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
</body>
</html>

