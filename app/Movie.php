<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Movie extends Model
{
    //gets a movie from WL with given user and movie id
	public static function getWatchLater($id, $movie){
		return \DB::table('watch_later')
        ->where([
            ['user_id' , '=' , $id],
            ['movie_id' , '=' , $movie],
        ])
        ->get();
	}
    //returns the category name with the given id
	public static function getMovieCategory($id){
		$category = \App\Categorie::where('id' , '=', $id)
                ->select('category_name')
                ->get();
        return   ucfirst($category[0]->category_name);
	}
    //gets movie with given id
	public static function getMovie($id){
		return \App\Movie::get()
            ->where('id' , '=' , $id)
            ->first();
	}
    //adds movie to watch latter
	public static function addWatchLater($movie){
		\DB::table('watch_later')
            ->insert([
                'user_id' => \Auth::user()->id,
                'movie_id' => $movie,
                'created_at' => Carbon::now()->toDateTimeString()
            ]);
	}

    //returns featured movies for the homepage
    public static function getFeaturedMovies(){
        return \DB::table('movies')->get()
        ->where('featured', '==', '1');
    }

}
