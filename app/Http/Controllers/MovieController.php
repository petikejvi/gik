<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Movie;
use App\History;
use App\Notification;
use Illuminate\Support\Facades\Redirect;

class MovieController extends Controller
{
    //middleware to prevent unauthenticated people to see the content
	public function __construct()
    {
        $this->middleware('auth');
    }

    //returns the data for he singlepage watch movie
    public function getMovie($id){

        //retrieves info from db
    	$movie = Movie::getMovie($id);
        //gets notificatons for the head of the page
    	$notifications = Notification::notifications();

        //checks if the movie exists
    	if(isset($movie->id))
        {
            //gets first category
            $category1 = Movie::getMovieCategory($movie->category1);
            //gets second category
            $category2 = Movie::getMovieCategory($movie->category2);
            //retrieves information from history table in db
            $check = History::checkHistory(\Auth::user()->id, $id);
            //checks if movie exists on history table in db
            if(count($check)==0){
                //adds movie to history list
                History::add($id);

            }
                //returns the view
        	   return view('movies.watch' , compact('notifications' , 'movie', 'category1', 'category2'));

    	}
    	else
        {
            //returns not found page
            return redirect('/search?search=sadf');
    	}
    }

    //add movie to watch later
    public function addWatchLater(Request $request){
        //gets from db the movie in watch later table
        $check = Movie::getWatchLater(\Auth::user()->id, $request->movie);
        //checks if movie is in watch later list
        if(count($check) != 0){
            //redirects back since it already exists
            return Redirect::back()->withErrors(['error' => __('movie.already_in_WL')]);
        }
        else{
            //adds movie to watch later list
            Movie::addWatchLater($request->movie);
            return Redirect::back()->withErrors(['success' => __('movie.added_to_WL')]);
        }

    }
}
