<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class SuggestionController extends Controller
{
    public function suggest(Request $request){

    	$toId = \DB::table('users')
    	->select('id')
    	->where('email' , '=' , $request->email)
    	->get();

    	if(count($toId) == 0){
    		return Redirect::back()->withErrors(['error' => 'Someone casted the Nox spell and HIMOS could not find the requested individual through the darkness!']);
    	}

    	$check = \DB::table('suggestions')
    	->where([
    	    		['to_id' , '=' , $toId['0']->id],
    	    		['movie_id' , '=' , $request->movie],
    	    	])
    	->get();

    	if(count($check) != 0){
    		return Redirect::back()->withErrors(['error' => 'HIMOS Dissendium spell showed you\'ve already been down this path and you have suggested this movie to the individual before!']);
    	}
    	else{
    		$check = \App\Friend::checkFriends($toId['0']->id);
    		if(count($check) != 0){
	    		\App\Suggestion::insert(
	    			['user_id' => \Auth::user()->id, 
	    			'to_id' => $toId['0']->id, 
	    			'movie_id' => $request->movie,
	    			'created_at' => Carbon::now()->toDateTimeString()
	    		]);
	    		return Redirect::back()->withErrors(['success' => 'HIMOS released it\'s Patronos and will show your friend the way to a wonderful movie!']);
	    	}
	    	else{
	    		return Redirect::back()->withErrors(['error' => 'HIMOS encountered a Protego Totalum around this person which suggest you are not friends!']);
	    	}
    	}

    }
}
