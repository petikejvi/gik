<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use App\Notification;
use App\Movie;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkAdm');
    }

    public function getAdmin(){


    	$admins = User::where('type', '=' , 1)->get();

        $notifications = Notification::notifications();

    	return view('admin.user' , compact( 'admins' , 'notifications' ));


    }

    public function addAdmin(Request $request){

    	$query = User::getUserFromEmail($request->email);
    	$query = User::where('id' , '=' , $query[0]->id)->get();
    	if($query[0]->id == \Auth::user()->id){
    		return Redirect::back()->withErrors(['error' => __('profile.admin_yourself')]);
    	}
    	if($query[0]->type == 1){
    		return Redirect::back()->withErrors(['error' => __('profile.already_admin')]);
    	}
    	else{
    		User::where('id' , $query[0]->id)->update(['type' => '1']);
    		return Redirect::back()->withErrors(['success' => __('profile.made_admin')]);
    	}
    }

    public function removeAdmin(Request $request){

    	if($request->id == \Auth::user()->id){
    	    return Redirect::back()->withErrors(['error' => __('profile.self_unadmin')]);    		
    	}
    	$query = User::where('id' , $request->id)->get();
    	if(count($query)!=0){
    	    User::where('id' , $request->id)->update(['type' => '0']);
    	    return Redirect::back()->withErrors(['success' => __('profile.remove_admin')]);
    	}
    	else{
    	    return Redirect::back()->withErrors(['error' => __('profile.unable_admin')]);    		
    	}
    }

    public function userData(){

        $notifications = Notification::notifications();

        $data[0] = count(User::where('type' , '0')->get());

        $data[1] = count(User::where('type' , '1')->get());

        $data[2] = count(\DB::table('movies')->get());

        $data[3] = count(\DB::table('friends')->get());

        $data[4] = count(\DB::table('suggestions')->get());

    	return view('admin.data' , compact( 'data' , 'notifications' ));
    }

    public function addMovies(){


        $notifications = Notification::notifications();

        return view('admin.addMovies' , compact( 'notifications' ));
    }

    public function insertMovies(Request $request){

        $this->validate($request, [
        'title' => 'required|max:255',
        'description' => 'required',
        'url' => 'required',
        'release' => 'required',
        'rating' => 'required',
        'length' => 'required',
        'category1' => 'required',
        'category2' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $image = $request->file('image');

        $input['imagename'] = time() .'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/img');

        $image->move($destinationPath, $input['imagename']);

        Movie::insert([
            'title' => $request->title,
            'description' => $request->description,
            'url' => $request->url,
            'release' => $request->release,
            'rating' => $request->rating,
            'length' => $request->length,
            'category1' => $request->category1,
            'category2' => $request->category2,
            'featured' => 0,
            'created_at' => Carbon::now()->toDateTimeString(),
            'image' => $input['imagename']
        ]);

        return Redirect::back()->withErrors(['success' => __('profile.movie_added')]);

    }

    public function reviewMovies(){

        $notifications = Notification::notifications();


        $featured = Movie::getFeaturedMovies();

        $movies = Movie::get();

        return view('admin.review' , compact('notifications' ,'featured' , 'movies' ));
    }

    public function featuredRemove(Request $request){

        Movie::where('id', $request->movie)
        ->update(['featured' => 0]);

        return Redirect::back()->withErrors(['success' => __('profile.featured_removed')]);

    }

    public function searchMovies(Request $request){

        $req = explode(" ", $request->search);
        
        $query = \DB::table('movies');
            foreach ($req as $word) {
                 $query->where(function ($query) use ($word)
                 {
                     $query->orWhere('title', 'like', '%' . $word . '%');
                 });
            }
         $movies = $query->get();        

        return view('layouts.partialMovies' , compact('movies'));


    }

    public function movieFeatured(Request $request){

        $check = Movie::where('featured' , '1')->get();
        if(count($check)<4){
            $check = Movie::where([
                            ['featured' , '=' , '1'] ,
                            ['id', '=' , $request->movie]
                        ])->get();
            if(count($check) == 0){
                Movie::where('id', $request->movie)
                ->update(['featured' => 1]);
                return Redirect::back()->withErrors(['success' => __('profile.featured_added')]);
                }
                else{
                    return Redirect::back()->withErrors(['error' => __('profile.already_added')]);
                }
        }
            return Redirect::back()->withErrors(['error' => __('profile.couldnt_add')]);

    }


    public function movieDelete(Request $request){

        Movie::where('id', $request->movie)->delete();

        return Redirect::back()->withErrors(['success' => __('profile.movie_deleted')]);


    }

    public function movieEdit(Request $request){

        $movie = Movie::where('id' , $request->movie)->get();
        $notifications = Notification::notifications();
        return view('admin.editMovie' , compact('movie', 'notifications'));

    }

    public function editData(Request $request){


        if($request->title){
            Movie::where('id' , $request->id)
            ->update(['title' => $request->title]);
        }
        if($request->description){
            Movie::where('id' , $request->id)
            ->update(['description' => $request->description]);            
        }
        if($request->url){
            Movie::where('id' , $request->id)
            ->update(['url' => $request->url]);
        }
        if($request->release){
            Movie::where('id' , $request->id)
            ->update(['release' => $request->release]);
        }
        if($request->rating){
            Movie::where('id' , $request->id)
            ->update(['rating' => $request->rating]);
        }
        if($request->length){
            Movie::where('id' , $request->id)
            ->update(['length' => $request->length]);
        }
        if($request->category1){
            Movie::where('id' , $request->id)
            ->update(['category1' => $request->category1]);
        }
        if($request->category2){
            Movie::where('id' , $request->id)
            ->update(['category2' => $request->category2]);
        }
        if($request->image){
        $this->validate($request, [
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $image = $request->file('image');

        $input['imagename'] = time() .'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/img');

        $image->move($destinationPath, $input['imagename']);
            Movie::where('id' , $request->id)
            ->update(['image' => $input['imagename']]);
        }

        return redirect()
        ->action('AdminController@reviewMovies')
        ->withErrors(['success' => __('profile.movie_updated')]);


    }

    public function sendMovieNotification(){

        Notification::sendMovieNotification();

        return Redirect::back()->withErrors(['success' => __('profile.notifications_sent')]);

    }


}
