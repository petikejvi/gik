<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Notification;
class SearchController extends Controller
{
     
	public function __construct()
    {
        $this->middleware('auth');
    }




    public function search(Request $request){
    	$req = explode(" ", $request->search);
    	
    	$query = \DB::table('movies');
        	foreach ($req as $word) {
    			 $query->where(function ($query) use ($word)
    			 {
    			     $query->orWhere('title', 'like', '%' . $word . '%');
    			 });
    		}
		$results = $query->get();

        $notifications = Notification::notifications();

    	return view('search.search', compact('results', 'notifications'));
    }




    public function category($category){


        $query = \App\Categorie::select('id')
        ->where('category_name' , '=' , $category)
        ->first();
        $secondary = \App\Movie::get()
        ->where('category2' , '=' , $query->id );
        $primary = \App\Movie::get()->
        where('category1' , '=' , $query->id )
        ->union($secondary);

        $notifications = Notification::notifications();
        return view('search.search_category' , compact('primary', 'notifications'));

    }
}
