<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Movie;
use \App\Notification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    //gets featured movies for the home screen
    public function home(){
        $featured = Movie::getFeaturedMovies();
        $notifications = Notification::notifications();
        return view('home' , compact('featured' , 'notifications'));
    }

}
