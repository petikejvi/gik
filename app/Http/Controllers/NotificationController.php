<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use User;
class NotificationController extends Controller
{
    public function __construct()
{ 
    $this->middleware('auth');
    $this->user = \Auth::user();
}
	//set notifications to seen
    public function resetNotifications()
    {
        $notifications = \App\Notification::find(1)->notifications();
        foreach ($notifications as $notification) {
           \DB::table('notifications')
                ->where('id', $notification->id)
                ->update(['read' => '1']);
        }
    }
}
