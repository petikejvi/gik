<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\Notification;
use App\Categorie;
use App\History;
use App\Friend;
use App\User;
class ProfileController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth');
    }
    //proffile page
    public function index(){
    //gets notifications
    $notifications = Notification::notifications();
    //gets categories for the user
    $category = Categorie::getCategories(\Auth::user()->id);
    //gets the history of the user
    $history = History::history(\Auth::user()->id);
    //gets the WL list of the user
    $watch = History::watchLater(\Auth::user()->id);
    //gets the suggested list
    $suggestions = History::suggested();
    //gets friends list
    $friends = Friend::getFriends();
    //returns the view
    return view('profile.profile' , compact('notifications','category' , 'history' , 'watch' , 'suggestions', 'friends'));

	}

    //change profile settings
    public function update(Request $request){

        if($request->password){
            if($request->password == $request->confirm_password){
                 \App\User::where('id' , '=' , \Auth::user()->id)
                 ->update(['password' => $request->password]);
            }
            else{ 
                return Redirect::back()->withErrors(['error' => __('profile.password_missmatch') ]);
            }
        }

        if($request->name){
            \App\User::where('id' , '=' , \Auth::user()->id)
            ->update(['name' => $request->name]);
        }
        if($request->email){
            \App\User::where('id' , '=' , \Auth::user()->id)
            ->update(['email' => $request->email]);
        }

        if($request->category1){
            \App\User::where('id' , '=' , \Auth::user()->id)
            ->update(['category1' => $request->category1]);
        }
        if($request->category2){
            \App\User::where('id' , '=' , \Auth::user()->id)
            ->update(['category2' => $request->category2]);
        }

        return Redirect::back()->withErrors(['success' => __('profile.update')]);

    }

    //sets friends
    public function setFriends(Request $request){
        //gets user from given email
        $query = User::getUserFromEmail($request->email);

        if(!isset($query[0]->id)){

            return Redirect::back()->withErrors(['error' => __('profile.not_found')]);
        }
        //checks if still friends
        $isFriend = Friend::checkFriends($query);

        if(isset($isFriend['0']->id)){

                return Redirect::back()->withErrors(['friends' => __('profile.already_friends')]);
        }
        else{
            if(\Auth::user()->id == $query[0]->id){

                return Redirect::back()->withErrors(['friends' => __('profile.self_friend')]);
            }
            else{

                Friend::addFriends($query);
                Notification::addFriend($query[0]->id);
                return Redirect::back()->withErrors(['friendship' => __('profile.befriended')]);
            }
        }

    }

    //removes friend from list
    public function removeFriend(Request $request){
        Friend::deleteFriend($request);
        return Redirect::back()->withErrors(['success' => __('profile.unfriend')]);

    }

    //accept friend request
    public function addFriend(Request $request){
        Friend::acceptFriendship($request->friend_id);
        return Redirect::back()->withErrors(['success' => __('profile.accept')]);
    }

    //sees friends profile
    public function friendProfile($id){

        $check = Friend::checkFriends($id);
        //checks if you are friends
        if(count($check) != 0){

            $friend = User::where('id' , '=' , $id)
            ->get();

            $notifications = Notification::notifications();

            $history = History::history($id);

            $watch = History::watchLater($id);

            $category = Categorie::getCategories($id);

            return view('profile.friend' , compact('friend' , 'notifications' , 'history' , 'watch', 'category' ));
        }
        //if not redirects back
        else{
            return Redirect::back()->withErrors(['error' => __('profile.notFound')]);
        }


    }

}