<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Friend extends Model
{
   static function getFriends(){
   	$query = \DB::table('friends')
        ->where('user2_id', '=', \Auth::user()->id)
        ->orWhere('user1_id', '=', \Auth::user()->id)
        ->get();
        $i = 0;
    if(count($query) != 0){
      foreach ($query as $key) {
            if($key->user1_id == \Auth::user()->id){
              $friends[$i]['id'] = $key->user2_id;
              $friends[$i]['name'] = User::getName($key->user2_id);
              $friends[$i]['status'] = User::friendshipCheck($key);
            }
            else{
              $friends[$i]['id'] = $key->user1_id;
              $friends[$i]['name'] = User::getName($key->user1_id);
              $friends[$i]['status'] = User::friendshipCheck($key); 
            }
            $i++;
          }
        }
      else{
        $freinds = [];
      }
      return $friends;
    }
    
   static function checkFriends($query){    
    if(isset($query[0]->id)){
        $value = \DB::table('friends')
            ->where([
                        ['user1_id', '=', $query[0]->id],
                        ['user2_id', '=', \Auth::user()->id],
                        ['status1', '=' , '1'],
                        ['status2', '=' , '1'],
                    ])
            ->orWhere([
                        ['user1_id', '=', \Auth::user()->id],
                        ['user2_id', '=', $query[0]->id],
                        ['status1', '=' , '1'],
                        ['status2', '=' , '1'],
                    ])
            ->get();
          }
    else{
      $value = \DB::table('friends')
        ->where([
                    ['user1_id', $query],
                    ['user2_id', \Auth::user()->id],
                    ['status1', '=' , '1'],
                    ['status2', '=' , '1'],
                ])
        ->orWhere([
                    ['user1_id', '=', \Auth::user()->id],
                    ['user2_id', '=', $query],
                    ['status1', '=' , '1'],
                    ['status2', '=' , '1'],
                ])
            ->get();
    }
        return $value;
  }

   static function addFriends($query){
        \DB::table('friends')
          ->insert(
              [
                  'user1_id' => \Auth::user()->id,
                  'user2_id' => $query[0]->id,
                  'status1' => '1',
                  'status2' => '0',
                  'created_at' => now(),
                  'updated_at' => now()
              ]);
        }




    static function deleteFriend($id){

      \DB::table('friends')
        ->where([
                    ['user1_id', '=', $id->friend_id],
                    ['user2_id', '=', \Auth::user()->id],
                ])
        ->orWhere([
                    ['user1_id', '=', \Auth::user()->id],
                    ['user2_id', '=', $id->friend_id],
                ])
        ->delete();

    }

    public static function acceptFriendship($friend_id){
      \DB::table('friends')
        ->where([
                    ['user1_id', '=', $friend_id],
                    ['user2_id', '=', \Auth::user()->id],
                ])
        ->orWhere([
                    ['user1_id', '=', \Auth::user()->id],
                    ['user2_id', '=', $friend_id],
                ])
        ->update(['status2' => '1']);
    }
}
