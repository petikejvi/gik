<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

	public function users(){
		return $this->belongsTo('App\User');
	}


	 public static function notifications()
	 {
	    $notif = User::find(1)->notifications()
	     	->take(5)
	     	->where('read' , '=' , '0')
	     	->orderBy('id' , 'desc')
	     	->get();
	    if(count($notif) == 0){	    
	     	$notif = User::find(1)->notifications()
	     	->take(5)
	     	->where('read' , '=' , '1')
	     	->orderBy('id' , 'desc')
	     	->get();
	     }
	     else{
	     	$notif = User::find(1)->notifications()
	     	->take(5)
	     	->orderBy('read' , 'asc')
	     	->orderBy('id' , 'desc')
	     	->get();
	     }
	     
	     return $notif;
	}

	public static function addFriend($id){

		\DB::table('notifications')
		->insert(
			['category' => '2',
			'message' => 'HIMOS recieved an owl with a frenship request!',
			'user_id' => $id,
			'link' => '',
			'read' => 0]
		);

	}

	public static function sendMovieNotification(){


		$users = User::get();

		foreach ($users as $user) {
		\DB::table('notifications')
		->insert(
			['category' => '1',
			'message' => 'HIMOS recieved new movies!',
			'user_id' => $user->id,
			'link' => url("/search?search=+"),
			'read' => 0]
		);
		}

		return "";


	}
}
