<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';

    public static function history($id){
    return \DB::table('history')
    ->join('movies' , 'history.movie_id' , '=' , 'movies.id')
    ->where('history.user_id' , '=' , $id)
    ->get();

    }

    public static function watchLater($id){
    return \DB::table('watch_later')
    ->join('movies' , 'watch_later.movie_id' , '=' , 'movies.id')
    ->where('watch_later.user_id' , '=' , $id)
    ->get();
    }

    public static function suggested(){
    $query = \DB::table('suggestions')
    ->join('movies' , 'suggestions.movie_id' , '=' , 'movies.id')
    ->where('suggestions.user_id' , '=' , \Auth::user()->id)
    ->join('users' , 'suggestions.to_id' , '=' , 'users.id')
    ->get();
    return $query;
    }

    public static function checkHistory($id, $movie){
        return \DB::table('history')
            ->select('id')
            ->where('user_id' , '=' , $id)
            ->where('movie_id' , '=' , $movie)
             ->get();
    }

    public static function add($movie){
        \DB::table('history')
        ->insert([
            'user_id' => \Auth::user()->id ,
            'movie_id' => $movie
         ]);
    }
}
