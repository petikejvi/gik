<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'category1', 'category2' , 'type'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public static function getName($id){

        $query = \DB::table('users')
        ->where('id' , '=' , $id)
        ->select('name')
        ->get();

        return $query[0]->name;

    }
    public static function friendshipCheck($key){

            if($key->user1_id == \Auth::user()->id && $key->status2 == 0){
             return 0; 
              //request sent, not yet accepted
            }
            else if($key->user1_id == \Auth::user()->id && $key->status2 == 1){
              return  1; 
              //request sent, accepted
            }
            else if($key->user2_id == \Auth::user()->id && $key->status2 == 1){
              return  1; 
              //request recieved, accepted
            }
            else{
              return  2;
              //request recieved, not yet accepted
            }
    }

    public static function getUserFromEmail($email){
        return \DB::table('users')
        ->where('email' , '=' , $email)
        ->select('id')
        ->get();
    }
}
