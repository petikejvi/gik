<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Movie;

class Categorie extends Model
{
    //returns the categories for a user with a given id
    public static function getCategories($id){
    $user = \DB::table('users')
    ->where('id' , '=' , $id)
    ->first();

    $category[0] = Movie::getMovieCategory($user->category1);

    $category[1] = Movie::getMovieCategory($user->category2);

    return $category;
    }
}
